---
layout: page
title: Lasersnijder
author: Raoul
date: 2021-07-06
topics: [lasercutting, designing]
image: /uploads/images/lasersnijder/trotec-speedy-300.png
---
Op The Space hebben we toegang tot de lasersnijder van Technolab dit is een Trotec Speedy 300.

![](/uploads/images/lasersnijder/trotec-speedy-300.png)

## Trotec Speedy 300
De Trotec speedy 300 is een CO2 lasersnijder met de volgende eigenschappen:
- Werkgebied: 726 x 432
- Max. werkstuk hoogte: 165 mm
- Laservermogen: 60-120 Watt

### Materialen overzicht
De Speedy 300 kan de volgende materialen bewerken (kruisje geeft aan dat het kan):

{:.table}
| Materiaal | Graveren | Snijden | Markeren |
| --- | --- | --- | --- |
| Acrylaat | x | x | - |
| Glas | x | - | - |
| Hout | x | x | - |
| Kunstof | x | x | - |
| Laminaat | x | x | - |
| Leer | x | x | - |
| Metaal | - | - | - |
| Papier | x | x | - |
| Steen | x | - | - |
| Textiel | x | x | - |

### Tooling
- Er is een hulpstuk voor het graferen van ronde materialen: [troteclaser.com/nl/lasermachines](https://www.troteclaser.com/nl/lasermachines/laser-accessoires/rondgraveermodule/)

# Belangrijke dingen VOORDAT je gaat snijden
1. Zet de punt afzuiging aan in de gang
2. Zet de lasersnijder aan (de knop zit links aan de achterkant van de lasersnijder)
3. Houd de klep gesloten tot het LED lampje tussen de 4 pijltjes knoppen langzaam begint te knipperen
4. Plaats je materiaal linksboven
5. Autofocus de laster (zie legenda knoppen)

## Voor de veiligheid
* Controleer of AirAssist aanstaat bij hout, karton en andere materialen die veel rook afgeven
* Gebruik nooit PVC of of een materiaal met andere chloorverbindingen. Bij deze materialen komt chloorgas vrij, dit roest de machine en is ook nog eens giftig.
* Blijf altijd bij de lasersnijder als deze aantstaat. Pauzeer een taak als je echt even weg moet.
* **Pauzeer de taak als er iets niet goed gaat of als je vlammen zien. De klep optillen stopt alle taken en werkt ook in geval van nood.**

## Legenda knoppen
![legenda_knoppen](/uploads/images/lasersnijder/legendaknoppen.png "Legenda Knoppen")

# Maken van eigen bestanden
De bestanden die je gebruikt voor de lasersnijder zijn Vector bestanden.
Je kunt deze maken met bijvoorbeeld het gratis programma [Inkscape](https://inkscape.org/) dit is opensource.

Andere mogelijke software paketten zijn (deze kunnen kosten met zich mee brengen):
- CoralDRAW
- Illustrator

Zolang het vector bestanden zijn kan het gebruikt worden. Wat er precies met welke kleuren gebeuren moet (snijden of graferen) kun je later kiezen in het Job Control programma. Als je het zo makkelijk mogelijk wilt houden is het het makkelijkste om alles zwart te maken wat je wilt graferen en alles de kleur rood te geven als je het wilt snijden.

# Bestand naar de lasersnijder sturen
Om met de printer de communiceren wordt gebruik gemaakt van het programma Trotec Job Control. Dit programma is aanwezig op een aantal van de laptops aanwezig van Technolab.
Neem vooral een USB stickje mee en dan kun je het makkelijk overzetten.

Open jouw bestand in bijvoorbeeld Inkscape en sluit de laptop aan op de kabel bij de lasersnijder. Druk vervolgens op printen. Je zult een scherm te zien krijgen waar je de lasersnijder kunt selecteren en tevens de instellingen kunt aanpassen.

![plaatje_printer_voorkeuren](/uploads/images/lasersnijder/printvoorkeuren_lasersnijder.png "Printvoorkeuren Lasersnijder")

Druk op instellignen aanpassen en controleer of ze als volgt ingesteld zijn:
* Width/Height: De afmetingen van wat je wilt printen. 740x440 is het maximale van de machine. Je hoeft deze instellingen niet aan te passen
* Minimize to Jobsize: Hier stuurt de software alleen door wat er daadwerkelijk gesneden of gegrafeerd moet worden, ook als je document andere afmetingen heeft
* Material Settings: Hier kies je de categorie en het specifieke materiaal.
    * P: Kracht van de laserbron in procenten
    * : Snelheid van de laser, trager = dieper snijden/graveren
    * Het knopje links bij Material Settings zijn geavanceerde instellingen, voor de dikte van het materiaal. Pas verder niets aan.
* Enchanced Geometries: Zorgt ervoor dat gebogen lijnen mooi gesneden worden, niet als een combinatie van korte rechte lijntjes
* Inner geometries first: Zorgt ervoor dat de lasersnijder van binnen naar buiten werkt, voorkomt dat een uitgesneden onderdeel valt en verplaatst voor het gegrafeerd (of verder gesneden) wordt

Als alles klopt druk je op de knop **JC** om te bevestingen. Pas hierna de voorkeuren toe en druk op print.

Pas dan start de Job Control Software

![plaatje_materiaal_voorkeuren](/uploads/images/lasersnijder/materiaalvoorkeuren_lasersnijder.png "Materiaalvoorkeuren Lasersnijder")

## Binnen JobControl
Als Totec JobControl gestart is komt jouw taak in het lijstje met taken.

![plaatje_jobcontrol](/uploads/images/lasersnijder/jobcontrol.png "JobControl")

* Material settings: Geavanceerde instellingen voor het materiaal. Als het goed is heb je dit al goed ingesteld in het vorige scherm
* Preview: wissel tussen een grijs rechthoek voor alle objecten of een voorbeeld van wat er echt gesneden gaat worden
* Positie voorwerp: Als de muis niet precies genoeg is, kun je zo het voorwerp precies plaatsen
* Settings & Printtijd: Overzicht van de printtijd per handeling. Klik op de Update knop voor een nieuwe berekening
* Printvoorbeeld: Afdrukvoorbeeld van wat er gaat gebeuren
* Taken: De lijst van alle ontwerpen die afgedrukt kunnen worden. Sleep ze met de muis naar het Printvoorbeeld
* Voorbeeld: Afdrukvoorbeeld van de geselecteerde taak
* Connect & Print: Maak verbinding met de lasersnijder (als je de USB kabel hebt aangesloten) en laat Printen
