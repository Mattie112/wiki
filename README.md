# Wiki
This is The Space Leiden wiki POC
Posts, Projects and tooling are added by creating a new markdown page in the folder

## Run wiki locally
If you want you can checkout this repo and run the wiki locally for development purposes.
You can use a pre-built docker image from jekyll:
```shell
docker run -v $(pwd):/srv/jekyll -p 4000:4000 -it jekyll/jekyll:4.2.0 jekyll serve
```